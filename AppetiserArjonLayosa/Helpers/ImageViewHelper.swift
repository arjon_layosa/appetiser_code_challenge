//
//  ImageViewHelper.swift
//  AppetiserArjonLayosa
//
//  Created by Arjon Layosa on 12/30/19.
//  Copyright © 2019 Arjon Layosa. All rights reserved.
//

import Foundation
import UIKit

extension UIImageView{
    func setImage(from url: String, completion: @escaping () -> Void) {
        //Get cache image if there is any
        if let image = UserDefaultsHelper.sharedInstance.imageCache.object(forKey: url as NSString){
            self.image = image
            completion()
        }
        else{
            //Download image
            guard let imageURL = URL(string: url) else { return }
            DispatchQueue.global().async {
                guard let imageData = try? Data(contentsOf: imageURL) else { return }
                let image = UIImage(data: imageData)
                //Set cache image
                UserDefaultsHelper.sharedInstance.imageCache.setObject(image ?? UIImage(named: "img_placeholder") ?? UIImage(), forKey: url as NSString)
                DispatchQueue.main.async {
                    self.image = image
                    completion()
                }
            }
        }
    
    }
}
