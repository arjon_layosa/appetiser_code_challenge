//
//  Constants.swift
//  AppetiserArjonLayosa
//
//  Created by Arjon Layosa on 12/19/19.
//  Copyright © 2019 Arjon Layosa. All rights reserved.
//

import UIKit

// MARK: - URL
public let ALL_LIST = "https://itunes.apple.com/search?term=star&amp;country=au&amp;media=movie&amp;all"


//MARK: - User Default Keys
public let LAST_VISITED_TRACK = "LAST_VISITED_TRACK"
public let SHOW_LAST_VISITED_TRACK = "SHOW_LAST_VISITED_TRACK"
