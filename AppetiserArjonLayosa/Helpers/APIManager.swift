//
//  APIManager.swift
//  AppetiserArjonLayosa
//
//  Created by Arjon Layosa on 12/19/19.
//  Copyright © 2019 Arjon Layosa. All rights reserved.
//

import UIKit
import Alamofire

class APIManager: NSObject {

    static let sharedInstance = APIManager()

    func getAllList(completion:@escaping (_ data: [TrackModel]?, _ error: String?) -> Void){
        AF.request(ALL_LIST, method: .get).responseDecodable { (response: DataResponse<AllListModel, AFError>) -> Void in
            switch response.result {
            case .success(let allListModel):
                completion(allListModel.results, nil)
            case .failure(let error):
                print("getAllList:error: \(error)")
            }
        }

    }
    
}
