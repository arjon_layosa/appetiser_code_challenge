//
//  UserDefaultsHelper.swift
//  AppetiserArjonLayosa
//
//  Created by Arjon Layosa on 12/30/19.
//  Copyright © 2019 Arjon Layosa. All rights reserved.
//

import UIKit

class UserDefaultsHelper: NSObject {
    //Image Cache
    let imageCache = NSCache<NSString, UIImage>()
    
    //Track
    static let sharedInstance = UserDefaultsHelper()
    let encoder = JSONEncoder()
    let defaults = UserDefaults.standard

    
    //MARK: Track Persistence
    func setLastVisitedTrack(track: TrackModel?){
        if let encoded = try? encoder.encode(track) {
            defaults.set(encoded, forKey: LAST_VISITED_TRACK)
        }
    }
    
    func getLastVisitedTrack() -> TrackModel?{
        if let data = defaults.object(forKey: LAST_VISITED_TRACK) as? Data {
            let decoder = JSONDecoder()
            if let track = try? decoder.decode(TrackModel.self, from: data) {
                return track
            }
        }
        return nil
    }

    //Only show last visited track if user quit the app while on the details view
    func setShouldShowLastVisitedTrack(show: Bool){
        defaults.set(show, forKey: SHOW_LAST_VISITED_TRACK)
    }

    func shoulShowLastVisitedTrack() -> Bool{
        return defaults.bool(forKey: SHOW_LAST_VISITED_TRACK)
    }
    
}
