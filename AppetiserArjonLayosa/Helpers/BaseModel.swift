//
//  BaseModel.swift
//  AppetiserArjonLayosa
//
//  Created by Arjon Layosa on 12/19/19.
//  Copyright © 2019 Arjon Layosa. All rights reserved.
//

import Foundation

struct AllListModel: Codable {
    var resultCount: Int?
    var results: [TrackModel]?
}

struct TrackModel: Codable{
    //List
    var trackName: String?
    var artworkUrl60: String?
    var trackPrice: Double?
    var primaryGenreName: String?
    
    //Details
    var artworkUrl100: String?
    var artistName: String?
    var collectionName: String?
    var releaseDate: String?
    var previewUrl: String?
    
}
