//
//  TrackDetailsViewController.swift
//  AppetiserArjonLayosa
//
//  Created by Arjon Layosa on 12/30/19.
//  Copyright © 2019 Arjon Layosa. All rights reserved.
//

import UIKit
import AVFoundation

class TrackDetailsViewController: UIViewController {
    
    @IBOutlet weak var btnPlay: UIButton!
    @IBOutlet weak var imgArtWork: UIImageView!
    @IBOutlet weak var lblArtistName: UILabel!
    @IBOutlet weak var lblCollectionName: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    
    var player: AVPlayer?

    var track: TrackModel?
    var placeHolder: UIImage?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.prefersLargeTitles = false
        self.title = self.track?.trackName ?? ""
        
        //Save current track as last visited track
        UserDefaultsHelper.sharedInstance.setLastVisitedTrack(track: track)
        
        self.setTrackDetails()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UserDefaultsHelper.sharedInstance.setShouldShowLastVisitedTrack(show: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if isMovingFromParent{
            //Set to false when going back to list
            UserDefaultsHelper.sharedInstance.setShouldShowLastVisitedTrack(show: false)
        }
    }
    @IBAction func buttonPlayTaped(_ sender: UIButton) {
        if player != nil{
            sender.setImage(UIImage(named: "img_play"), for:   .normal)
            player?.pause()
            player = nil
        }else{
            if let url = URL(string: self.track?.previewUrl ?? ""){
                print("url:\(url)")
                let playerItem: AVPlayerItem = AVPlayerItem(url: url)
                player = AVPlayer(playerItem: playerItem)
                
                let playerLayer = AVPlayerLayer(player: player)
                
                playerLayer.frame = CGRect(x: 0, y: 0, width: 10, height: 50)
                self.view.layer.addSublayer(playerLayer)
                player?.play()
                sender.setImage(UIImage(named: "img_pause"), for:   .normal)

            }
        }
      

    }
}

extension TrackDetailsViewController{
    func setTrackDetails(){
        
        self.lblArtistName.text = self.track?.artistName ?? ""
        self.lblCollectionName.text = self.track?.collectionName ?? ""
        self.lblPrice.text = "$\(self.track?.trackPrice ?? 0)"
        
        //Date
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        if let strDate = self.track?.releaseDate, let date = dateFormatter.date(from: strDate) {
            dateFormatter.dateFormat = "MMM dd, yyyy"
            self.lblDate.text = dateFormatter.string(from: date)
        } else {
            print("There was an error decoding the string")
        }
        
        self.imgArtWork.image = self.placeHolder
        //Download Image 100x100
        if let strUrl = self.track?.artworkUrl100{
            self.imgArtWork.setImage(from: strUrl) {
                self.btnPlay.isHidden = false
            }
        }
    }
}
