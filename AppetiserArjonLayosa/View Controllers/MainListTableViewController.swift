//
//  MainListTableViewController.swift
//  AppetiserArjonLayosa
//
//  Created by Arjon Layosa on 12/19/19.
//  Copyright © 2019 Arjon Layosa. All rights reserved.
//

/*
 
 Architecture uses is MVC since the app is very simple.
 Doing MVVM or others might result to over architecture
 
 */

import UIKit

class MainListTableViewController: UITableViewController {
    
    var trackList: [TrackModel] = []
    var lastVisitedTrack: TrackModel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Gets the last track visited and show its details
        if UserDefaultsHelper.sharedInstance.shoulShowLastVisitedTrack(), let track = UserDefaultsHelper.sharedInstance.getLastVisitedTrack(){
            self.showDetailedViewController(track)
        }
        
        APIManager.sharedInstance.getAllList { (data, error) in
            if let trackList = data{
                self.trackList = trackList
                self.tableView.reloadData()
            }
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.prefersLargeTitles = true
        
        //Refresh to show last visited track when going back from details page
        self.tableView.reloadData()
    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        if let lastVisitedTrack = UserDefaultsHelper.sharedInstance.getLastVisitedTrack(){
            self.lastVisitedTrack = lastVisitedTrack
            return 2
        }else{
            return 1
        }
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0{
            return "Last Visited Track"
        }else{
            return "All Tracks"
        }
    }
    

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 1
        }
        return self.trackList.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MainListTableViewCell", for: indexPath) as! MainListTableViewCell

        if indexPath.section == 0{
            cell.track = self.lastVisitedTrack
        }else{
            let track = self.trackList[indexPath.row]
            cell.track = track
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
 
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var track = self.trackList[indexPath.row]
        var placeHolder: UIImage?

        if indexPath.section == 0{
            track = self.lastVisitedTrack
        }
        
        //Set placeholder of the smaller artwork to show on details
        //Download the larger art work on details page itself
        if let cell = tableView.cellForRow(at: indexPath) as? MainListTableViewCell{
            placeHolder = cell.imgArtWork.image
        }
        self.showDetailedViewController(track, placeHolder)
    }
}


extension MainListTableViewController{
    func showDetailedViewController(_ track: TrackModel, _ placeHolder: UIImage? = UIImage(named: "img_placeholder")){
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let trackDetailsViewController = storyBoard.instantiateViewController(withIdentifier: "TrackDetailsViewController") as! TrackDetailsViewController
        trackDetailsViewController.track = track
        trackDetailsViewController.placeHolder = placeHolder
        self.navigationController?.pushViewController(trackDetailsViewController, animated: true)
    }
}
