//
//  MainListTableViewCell.swift
//  AppetiserArjonLayosa
//
//  Created by Arjon Layosa on 12/20/19.
//  Copyright © 2019 Arjon Layosa. All rights reserved.
//

import UIKit

class MainListTableViewCell: UITableViewCell {
    @IBOutlet weak var imgArtWork: UIImageView!
    @IBOutlet weak var lblTrackName: UILabel!
    @IBOutlet weak var lblGenre: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    
    var track: TrackModel?{
        didSet{
            self.lblTrackName.text = self.track?.trackName ?? ""
            self.lblGenre.text = self.track?.primaryGenreName ?? ""
            self.lblPrice.text = "$\(self.track?.trackPrice ?? 0)"
            self.imgArtWork.image = UIImage(named: "img_placeholder")
            if let strUrl = self.track?.artworkUrl60{
                self.imgArtWork.setImage(from: strUrl) {}
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
